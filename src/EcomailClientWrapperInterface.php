<?php

namespace Drupal\ecomail;

/**
 * Interface EcomailInterface.
 *
 * @package Drupal\ecomail
 */
interface EcomailClientWrapperInterface {

  /**
   * Return list of collection.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list-collections/view-all-lists
   */
  public function getListsCollection();

  /**
   * Add a new list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list-collections/add-new-list
   */
  public function addListCollection(array $data);

  /**
   * Return list detail.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list/show-list
   */
  public function showList($list_id);

  /**
   * Update list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list/update-list
   */
  public function updateList($list_id, array $data);

  /**
   * Get subscribers of a list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list-subscribers/get-subscribers
   */
  public function getSubscribers($list_id);

  /**
   * Get detailed data about subscriber based on list and e-mail.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/get-subscriber/get-subscriber
   */
  public function getSubscriber($list_id, $email);

  /**
   * Get detailed data about subscriber based on e-mail.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/subscribers/get-subscriber/get-subscriber-details
   */
  public function getSubscriberList($email);

  /**
   * Add subscriber to list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list-subscribe/add-new-subscriber-to-list
   */
  public function addSubscriber($list_id, array $data);

  /**
   * Remove subscriber from the list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/subscribers/delete-subscriber/remove-subscriber-from-db
   */
  public function removeSubscriber($list_id, array $data);

  /**
   * Update subscriber data.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/subscriber-update/update-subscriber
   */
  public function updateSubscriber($list_id, array $data);

  /**
   * Add multiple subscribers to list.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/lists/list-subscribe-bulk/add-bulk-subscribers-to-list
   */
  public function addSubscriberBulk($list_id, array $data);

  /**
   * Delete subscriber.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/subscribers/delete-subscriber/remove-subscriber-from-db
   */
  public function deleteSubscriber($email);

  /**
   * Get subscriber information by email (global).
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/subscribers/get-subscriber/get-subscriber-details
   */
  public function getSubscriberByEmail($email);

  /**
   * List campaigns.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/campaigns-collection/list-all-campaigns
   */
  public function listCampaigns($filters = NULL);

  /**
   * Add campaign.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/campaigns-collection/add-new-campaign
   */
  public function addCampaign(array $data);

  /**
   * Update campaign.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/campaign/update-campaign
   */
  public function updateCampaign($campaign_id, array $data);

  /**
   * Send campaign.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/send-campaign/send-campaign
   */
  public function sendCampaign($campaign_id);

  /**
   * Get campaign statistics for specified campaign.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/get-campaign-stats/get-campaign-stats
   */
  public function getCampaignStats($campaign_id);

  /**
   * Get detailed campaign stats by campaign ID with optional query parameters.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/campaigns/get-campaign-stats-detail/get-campaign-stats-detail
   */
  public function getCampaignStatsDetail($campaignId, $queryParams = []);

  /**
   * List automations.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/automations/automations-collection/list-all-automations
   */
  public function listAutomations();

  /**
   * Trigger automation.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/automations/trigger-automation/trigger-automation
   */
  public function triggerAutomation($automation_id, array $data);

  /**
   * Get automation statistics.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/automations/get-automation-stats/get-automation-stats
   */
  public function getPipelineStats($pipelineId);

  /**
   * Get detailed state of an automation pipeline by ID, with optional params.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/automations/get-automation-stats-detail/get-automation-stats-detail
   */
  public function getPipelineStatsDetail($pipelineId, $queryParams = []);

  /**
   * Create template.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/templates/template-collection/create-a-new-template
   */
  public function createTemplate(array $data);

  /**
   * List domains associted with the account.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/domains/domains-collection/list-all-domains
   */
  public function listDomains();

  /**
   * Create domain.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/domains/domains-collection/create-a-new-domain
   */
  public function createDomain(array $data);

  /**
   * Delete domain.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/domains/delete-a-domain
   */
  public function deleteDomain($id);

  /**
   * Send transactional email.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactional-emails/send-transactional-email/send-transactional-email
   */
  public function sendTransactionalEmail(array $data);

  /**
   * Send transactional template.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactional-emails/send-transactional-template/send-transactional-email
   */
  public function sendTransactionalTemplate(array $data);

  /**
   * Stats for transactional emails for the past 6 months.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactional-emails/transactional-emails-stats/transactional-emails-stats
   */
  public function getTransactionalStats();

  /**
   * Stats for double opt-in emails for the past 6 months.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactional-emails/double-opt-in-stats/double-opt-in-stats
   */
  public function getTransactionalStatsDoi();

  /**
   * Create new transaction.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactions/add-transaction/create-a-new-transaction
   */
  public function createNewTransaction(array $data);

  /**
   * Create bulk transaction.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactions/add-bulk-transactions/create-bulk-transactions
   */
  public function createBulkTransactions(array $data);

  /**
   * Update transaction.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactions/update-transaction/update-transaction
   */
  public function updateTransaction($transaction_id, array $data);

  /**
   * Delete transaction.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/transactions/delete-transaction/remove-transaction-from-db
   */
  public function deleteTransaction($transaction_id);

  /**
   * Update a product feed.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/feeds/product-feeds/update-a-product-feed
   */
  public function refreshProductFeed($feedId);

  /**
   * Update a data feed.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/feeds/data-feeds/update-a-data-feed
   */
  public function refreshDataFeed($feedId);

  /**
   * Create a new event.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/tracker/events/create-a-new-event
   */
  public function addEvent(array $data);

  /**
   * Search contact.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/search/search/find-contact
   */
  public function search($query);

  /**
   * Import discount coupons.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/discount-coupons/import/import-discount-coupons
   */
  public function importCoupons(array $data);

  /**
   * Delete discount coupons.
   *
   * @see https://ecomailczv2.docs.apiary.io/#reference/discount-coupons/delete/delete-discount-coupons
   */
  public function deleteCoupons(array $data);

}
