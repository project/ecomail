<?php

namespace Drupal\ecomail;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\key\KeyRepositoryInterface;

/**
 * Class EcomailClientWrapper.
 *
 * Provides wrapper for ecomail php wrapper to be used as Drupal service.
 */
class EcomailClientWrapper implements EcomailClientWrapperInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Ecomail php wrapper.
   *
   * @var \Ecomail
   *
   * @see https://github.com/Ecomailcz/ecomail-php
   */
  protected $ecomailPhpWrapper;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Drupal\key\KeyRepositoryInterface.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Ecomail wrapper constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Config.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   Key.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger.
   */
  public function __construct(ConfigFactoryInterface $config, KeyRepositoryInterface $key_repository, LoggerChannelFactoryInterface $logger_factory) {
    $this->config = $config->get('ecomail.settings');
    $this->keyRepository = $key_repository;
    $this->logger = $logger_factory;
    $this->ecomailPhpWrapper = new \Ecomail($this->getApiKey());
  }

  /**
   * {@inheritdoc}
   */
  public function getListsCollection() {
    return $this->processResponse($this->ecomailPhpWrapper->getListsCollection());
  }

  /**
   * {@inheritdoc}
   */
  public function addListCollection(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->addListCollection($data));
  }

  /**
   * {@inheritdoc}
   */
  public function showList($list_id) {
    return $this->processResponse($this->ecomailPhpWrapper->showList($list_id));
  }

  /**
   * {@inheritdoc}
   */
  public function updateList($list_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->updateList($list_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscribers($list_id) {
    return $this->processResponse($this->ecomailPhpWrapper->getSubscribers($list_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriber($list_id, $email) {
    return $this->processResponse($this->ecomailPhpWrapper->getSubscriber($list_id, $email));
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriberList($email) {
    return $this->processResponse($this->ecomailPhpWrapper->getSubscriberList($email));
  }

  /**
   * {@inheritdoc}
   */
  public function addSubscriber($list_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->addSubscriber($list_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function removeSubscriber($list_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->removeSubscriber($list_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function updateSubscriber($list_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->updateSubscriber($list_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function addSubscriberBulk($list_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->addSubscriberBulk($list_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSubscriber($email) {
    return $this->processResponse($this->ecomailPhpWrapper->deleteSubscriber($email));
  }

  /**
   * {@inheritdoc}
   */
  public function getSubscriberByEmail($email) {
    return $this->processResponse($this->ecomailPhpWrapper->getSubscriberByEmail($email));
  }

  /**
   * {@inheritdoc}
   */
  public function listCampaigns($filters = NULL) {
    return $this->processResponse($this->ecomailPhpWrapper->listCampaigns());
  }

  /**
   * {@inheritdoc}
   */
  public function addCampaign(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->addCampaign($data));
  }

  /**
   * {@inheritdoc}
   */
  public function updateCampaign($campaign_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->updateCampaign($campaign_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function sendCampaign($campaign_id) {
    return $this->processResponse($this->ecomailPhpWrapper->sendCampaign($campaign_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getCampaignStats($campaign_id) {
    return $this->processResponse($this->ecomailPhpWrapper->getCampaignStats($campaign_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getCampaignStatsDetail($campaignId, $queryParams = []) {
    return $this->processResponse($this->ecomailPhpWrapper->getCampaignStatsDetail($campaignId));
  }

  /**
   * {@inheritdoc}
   */
  public function listAutomations() {
    return $this->processResponse($this->ecomailPhpWrapper->listAutomations());
  }

  /**
   * {@inheritdoc}
   */
  public function triggerAutomation($automation_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->triggerAutomation($automation_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function getPipelineStats($pipelineId) {
    return $this->processResponse($this->ecomailPhpWrapper->getPipelineStats($pipelineId));
  }

  /**
   * {@inheritdoc}
   */
  public function getPipelineStatsDetail($pipelineId, $queryParams = []) {
    return $this->processResponse($this->ecomailPhpWrapper->getPipelineStatsDetail($pipelineId));
  }

  /**
   * {@inheritdoc}
   */
  public function createTemplate(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->createTemplate($data));
  }

  /**
   * {@inheritdoc}
   */
  public function listDomains() {
    return $this->processResponse($this->ecomailPhpWrapper->listDomains());
  }

  /**
   * {@inheritdoc}
   */
  public function createDomain(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->createDomain($data));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDomain($id) {
    return $this->processResponse($this->ecomailPhpWrapper->deleteDomain($id));
  }

  /**
   * {@inheritdoc}
   */
  public function sendTransactionalEmail(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->sendTransactionalEmail($data));
  }

  /**
   * {@inheritdoc}
   */
  public function sendTransactionalTemplate(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->sendTransactionalTemplate($data));
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionalStats() {
    return $this->processResponse($this->ecomailPhpWrapper->getTransactionalStats());
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionalStatsDoi() {
    return $this->processResponse($this->ecomailPhpWrapper->getTransactionalStatsDOI());
  }

  /**
   * {@inheritdoc}
   */
  public function createNewTransaction(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->createNewTransaction($data));
  }

  /**
   * {@inheritdoc}
   */
  public function createBulkTransactions(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->createBulkTransactions($data));
  }

  /**
   * {@inheritdoc}
   */
  public function updateTransaction($transaction_id, array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->updateTransaction($transaction_id, $data));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTransaction($transaction_id) {
    return $this->processResponse($this->ecomailPhpWrapper->deleteTransaction($transaction_id));
  }

  /**
   * {@inheritdoc}
   */
  public function refreshProductFeed($feedId) {
    return $this->processResponse($this->ecomailPhpWrapper->refreshProductFeed($feedId));
  }

  /**
   * {@inheritdoc}
   */
  public function refreshDataFeed($feedId) {
    return $this->processResponse($this->ecomailPhpWrapper->refreshDataFeed($feedId));
  }

  /**
   * {@inheritdoc}
   */
  public function addEvent(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->addEvent($data));
  }

  /**
   * {@inheritdoc}
   */
  public function search($query) {
    return $this->processResponse($this->ecomailPhpWrapper->search($query));
  }

  /**
   * {@inheritdoc}
   */
  public function importCoupons(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->importCoupons($data));
  }

  /**
   * {@inheritdoc}
   */
  public function deleteCoupons(array $data) {
    return $this->processResponse($this->ecomailPhpWrapper->deleteCoupons($data));
  }

  /**
   * Helper method to process response from API.
   *
   * @param mixed $response
   *   Response from API call.
   *
   * @return mixed|null
   *   Return decoded data or NULL.
   */
  protected function processResponse($response) {
    if (is_array($response) || is_object($response)) {
      return $response;
    }
    if (is_string($response)) {
      $decoded_response = Json::decode($response);

      if ($decoded_response) {
        return $decoded_response;
      }
    }

    $this->logger->get('ecomail')->error('Response from API: ' . Json::encode($response));
    return NULL;
  }

  /**
   * Get api key from module configuration.
   */
  protected function getApiKey() {
    $key_name = $this->config->get('api_key');
    return $this->keyRepository->getKey($key_name)->getKeyValue();
  }

}
